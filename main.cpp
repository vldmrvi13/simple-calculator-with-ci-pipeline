#include <iostream>
#include <chrono>
#include <thread>
#include "calculator.h"


int main() {
    Calculator calc;
    double a, b;
    char operation;

    std::cout << "Enter first number: ";
    std::cin >> a;
    std::cout << "Enter operation (+, -, *, /): ";
    std::cin >> operation;
    std::cout << "Enter second number: ";
    std::cin >> b;

    try {
        double result;
        switch (operation) {
            case '+':
                result = calc.add(a, b);
                break;
            case '-':
                result = calc.subtract(a, b);
                break;
            case '*':
                result = calc.multiply(a, b);
                break;
            case '/':
                result = calc.divide(a, b);
                break;
            default:
                std::cerr << "Invalid operation!" << std::endl;
                return 1;
        }
        std::cout << "Result: " << result << std::endl;
    } catch (const std::exception &e) {
        std::cerr << "Error: " << e.what() << std::endl;
        return 1;
    }
    std::this_thread::sleep_for(std::chrono::seconds(3));
    return 0;
}
