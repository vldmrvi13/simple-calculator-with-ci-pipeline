#include <gtest/gtest.h>
#include "calculator.h"

TEST(CalculatorTest, Add) {
    Calculator calc;
    EXPECT_DOUBLE_EQ(calc.add(1, 2), 3);
    EXPECT_DOUBLE_EQ(calc.add(-1, -2), -3);
    EXPECT_DOUBLE_EQ(calc.add(-1, 1), 0);
}

TEST(CalculatorTest, Subtract) {
    Calculator calc;
    EXPECT_DOUBLE_EQ(calc.subtract(3, 2), 1);
    EXPECT_DOUBLE_EQ(calc.subtract(-1, -2), 1);
    EXPECT_DOUBLE_EQ(calc.subtract(1, -1), 2);
}

TEST(CalculatorTest, Multiply) {
    Calculator calc;
    EXPECT_DOUBLE_EQ(calc.multiply(3, 2), 6);
    EXPECT_DOUBLE_EQ(calc.multiply(-1, -2), 2);
    EXPECT_DOUBLE_EQ(calc.multiply(-1, 1), -1);
}

TEST(CalculatorTest, Divide) {
    Calculator calc;
    EXPECT_DOUBLE_EQ(calc.divide(4, 2), 2);
    EXPECT_DOUBLE_EQ(calc.divide(-4, -2), 2);
    EXPECT_DOUBLE_EQ(calc.divide(-4, 2), -2);
}

TEST(CalculatorTest, DivideByZero) {
    Calculator calc;
    EXPECT_THROW(calc.divide(1, 0), std::invalid_argument);
}

int main(int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
